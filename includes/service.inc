<?php

/**
 * JODConverter class
 */
class JODConverter {

  /**
   * Convert string data
   *
   * @param string $inputData
   *   The data to convert
   * @param $inputType
   *   The converted data
   * @param string $outputType
   *   The requested data mimetype
   */
  public static function convertData($inputData, $inputType, $outputType) {

    // HTTP request options
    $url = variable_get('jodconverter_service_url', 'http://localhost:8080/converter/service');
    $options = array(
      'method' => 'POST',
      'data' => $inputData,
      'headers' => array(
        'Content-Type' => $inputType,
        'Accept' => $outputType,
      )
    );

    // Send request
    $response = drupal_http_request($url, $options);

    return $response->code == 200 ? $response->data : FALSE;
  }

  /**
   * Convert a file 
   *
   * @param string $inputFile
   *   The file path to convert
   * @param $outputFile
   *   The requested converted file destination
   * @param string $replace
   *   Replace behavior when the destination file already exists
   *
   * @return object | bool
   * The converted file
   */
  public static function convertFile($inputFile, $outputFile, $replace = FILE_EXISTS_RENAME) {
    // check user rights & file 
    if(!user_access('use jodconverter') || !file_exists($inputFile)) {
      return FALSE;
    }

    // get file data
    $inputData = file_get_contents($inputFile);

    // get requested mime types
    $inputType = file_get_mimetype($inputFile);
    $outputType = file_get_mimetype($outputFile); 

    // converter data
    $outputData = self::convertData($inputData, $inputType, $outputType);

    // Create converted file
    return $outputData !== FALSE ? file_save_data($outputData, $outputFile, $replace) : FALSE;
  }

  /**
   * Convert an unmanaged file
   *
   * @param string $inputFile
   *   The file path to convert
   * @param $outputFile
   *   The requested converted file destination
   * @param string $replace
   *   Replace behavior when the destination file already exists
   *
   * @return string | bool
   * The converted file path
   * 
   */
  public static function convertUnmanagedFile($inputFile, $outputFile, $replace = FILE_EXISTS_RENAME) {
    // check user rights & file 
    if(!user_access('use jodconverter') || !file_exists($inputFile)) {
      return FALSE;
    }

    // get file data
    $inputData = file_get_contents($inputFile);

    // get requested mime types
    $inputType = file_get_mimetype($inputFile);
    $outputType = file_get_mimetype($outputFile); 

    // converter data
    $outputData = self::convertData($inputData, $inputType, $outputType);

    // Create converted unmanaged file
    return $outputData !== FALSE ? file_unmanaged_save_data($outputData, $outputFile, $replace) : FALSE;
  }

  /**
   * Ping JODConverter service
   *
   * @return array
   * The request response
   */
  public static function ping() {

    // HTTP request options
    $url = variable_get('jodconverter_service_url', 'http://localhost:8080/converter/service');
    $options = array(
      'method' => 'POST',
      'data' => 'ping',
      'headers' => array(
        'Content-Type' => 'text/plain',
        'Accept' => 'text/plain',
      )
    );

    // Send request
    return drupal_http_request($url, $options);
  }
}
