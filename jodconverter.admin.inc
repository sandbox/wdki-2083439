<?php

/**
 * @file
 * Admin page callbacks for the Search API Solr module.
 */

/**
 * Form constructor for the Solr files overview.
 *
 * @param SearchApiServer $server
 *   The server for which files should be displayed.
 *
 * @ingroup forms
 */
function jodconverter_config_form($form, &$form_state) {
  $form['jodconverter_service_url'] = array(
    '#type' => 'textfield',
    '#title' => t('JODConverter service url'),
    '#default_value' => variable_get('jodconverter_service_url', 'http://localhost:8080/converter/service'),
    '#description' => t('Enter the JODConverter service url.'),
  );

  return $form;
}
