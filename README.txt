README.txt
==========

The purpose of this module is to ffers an implementation of JODConverter.
This module does nothing itself and only provides API for convert files
using JODConverter.

Installation
------------

In order for this module to work, you will first need to set up JODConverter.
Please follow the instructions detailed at [1].

Go to admin/config/services/jodconverter to set up your JODConverter service url.

[1] http://www.artofsolving.com/node/14

Usage
-----

Convert a file:
$converted_filepath = JODConverter::convertUnmanagedFile('public://doc.odt', 'public://doc.html');

Convert an unmanaged file:
$converted_file = JODConverter::convertFile('public://doc.docx', 'public://doc.pdf');

Supported Formats
-----------------

You can check supported formats at [1]

[1] http://www.artofsolving.com/opensource/jodconverter/guide/supportedformats

